# StructureForge

A small scripting library for preparing fingerprints from DFT, MD or other simulations as input for machine learning methods.
The fingerprint is a class. Different functions are necessary or customizable for reading input from different sources.

## Output format

Each fingerprint is wrapped in `begin` and `end` keywords. Comments can be added through preceding a line with the letter `c`. Other keywords are given in the table:

| Keyword   | Values                            | Description                                                                                      |
|-----------|-----------------------------------|--------------------------------------------------------------------------------------------------|
| `lattice` | x, y, z                           | lattice vectors [Bohr]; not necessarily orthogonal; 3 vectors                                    |
| `atom`    | x, y, z, type, e1, e2, fx, fy, fz | atom position [Bohr], identifier, energies (not yet, put `0`) [Hartree] and forces [Hartree/bar] |
| `energy`  | E                                 | total energy of fingerprint [Hartree]                                                            |
| `charge`  | C                                 | charge of fingerprint not yet, put `0.0`                                                         |

Note: depending on the training suite for Behler-Parinello type potentials (either `RuNNer` or `n2p2`), the units have to be specified or can be chosen freely.

The `lattice` commands give the units of the lattice for possible periodicity. They do not necessarily have to be orthonormal; distance units are in [Bohr].

Data of individual atoms is added with a preceding `atom` keyword, followed by x, y, z position [Bohr] an identifier for the element, e.g. Al, two `0.0` for a future energy reference; units in [Hartree]. Last three columns are force components in x, y, z [Hartree/bar]. If not present, set zero.

Example (arbitrary values):
```
begin
c example dataset 1, snapshot 1
lattice 7.710747 0.000000 0.000000
lattice 0.000000 7.710747 0.000000
lattice 0.000000 0.000000 7.710747
atom  0.000000 3.855374 3.855374  Al 0.0000 0.0000  7.39586456e-03 1.80543989e-03 1.43222992e-03
atom  4.132058 0.187160 3.842285  Al 0.0000 0.0000  -1.08302394e-02 -3.35073480e-03 2.14079987e-03
atom  3.945636 4.059045 7.599908  Al 0.0000 0.0000  -1.35903992e-03 -8.48877950e-03 3.51054979e-03
atom  0.104525 7.633332 0.121226  Mg 0.0000 0.0000  4.79341472e-03 1.00340744e-02 -7.08358458e-03
energy -7.02227168e+01
charge 0.0000
end
```
