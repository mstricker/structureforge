#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@file   collect_structure_y_dir.py

@author Markus Stricker <markus.stricker@epfl.edu>

@date   2018-04-05

@brief  example for scanning folders for structures

@section LICENCE

Copyright (C) 2018 Markus Stricker

collect_structure_y_dir.py is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

collect_structure_y_dir.py is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licences/>.
"""

import os

import StructureForge.InputTools.VASPReader as vr

# using the test data to show a usage scenario
# adjust your folders for simulations to your case
basefolder = os.getcwd() + '/y_dir/'

folders = [f + '/' for f in os.listdir(basefolder)
           if os.path.isdir(basefolder + f)]

coll = []

# collect structures
for f in folders:
    fp_tmp = vr.reader(basefolder + f)
    coll.append(fp_tmp)

# define output file
outf = 'structures.dat'

# make sure file is empty
if outf in os.listdir(os.getcwd()):
    os.remove(outf)

# write structures
for x in coll:
    x.write(outf, writepolicy='a')
