#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@file   read_structure_file.py

@author Markus Stricker <markus.stricker@epfl.edu>

@date   2018-05-02

@brief  example for getting a single fingerprint from directory

@section LICENCE

Copyright (C) 2018 Markus Stricker

StructureForge is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

StructureForge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licences/>.
"""

import StructureForge.InputTools.FPTextReader as tr

# using the test data to show a usage scenario
# adjust your folders for simulations to your case
filename = "../tests/Text_example/AtomicStructures.dat"

print ('reading from', filename)

collection = tr.reader(filename)

for fp in collection:
    fp.print_stdout()
