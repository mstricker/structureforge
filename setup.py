#!/usr/bin/env python
"""
Setup script for Python StructureForge
Started by M. Stricker, 2018

install as
python setup.py install --u
"""

from setuptools import setup, find_packages
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))


setup(name='StructureForge',
      version='0.1',

      description='Scripts for creating fingerprints as input for machine learning algorithms',

      # URL
      url="https://gitlab.com/mstricker/structureforge",

      # Author
      author="Markus Stricker",
      author_email='mail@markusstricker.de',

      license='MIT',

      classifiers=[
          # How mature is this project? Common values are
          # 3 - Alpha
          # 4 - Beta
          # 5 - Production/Stable
          'Development Status :: 3 - Alpha'

          'License :: OSI Approved :: MIT License'

      ],

      keywords='preprocessing, machine learning, structures, atoms',

      packages=find_packages(),
      )


# python3 setup.py install --u
