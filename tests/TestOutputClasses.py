#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@file   02_TestOutputClasses.py

@author Markus Stricker <markus.stricker@epfl.edu>

@date   2018-03-28

@brief  test suite to check classes of atomic finger print

@section LICENCE

Copyright (C) 2018 Markus Stricker

02_TestOutputClasses.py is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

02_TestOutputClasses.py is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licences/>.
"""

try:
    import unittest
    import numpy as np
    import filecmp

    import StructureForge.OutputTools.AtomicStructure as fp

except ImportError as err:
    import sys
    print(err)
    sys.exit(-1)


class TestAtomicStructureClasses(unittest.TestCase):
    def setUp(self):
        # for coordinate system
        self.e1 = np.array([1., 0., 0.])
        self.e2 = np.array([0., 1., 0.])
        self.e3 = np.array([0., 0., 1.])
        self.refCsString = ("lattice 1.00000000 0.00000000 0.00000000\n"
                            "lattice 0.00000000 1.00000000 0.00000000\n"
                            "lattice 0.00000000 0.00000000 1.00000000\n")
        # for atom
        self.Pos = np.array([1., 1., 1.])
        self.AtomType = 'H'
        self.elements = ['H']
        self.c = [7., 11.]
        self.Force = np.array([1., 2., 3.])
        self.refAtomString = 'atom  ' \
            + np.array2string(self.Pos,
                              formatter={'float_kind': lambda x: "%.8f" % x},
                              separator=' ')[1:-1].strip() \
            + '  ' + self.AtomType + ' ' + str(self.c[0]) + ' ' \
            + str(self.c[1]) + '  ' \
            + np.array2string(self.Force,
                              formatter={'float_kind': lambda x: "%.8e" % x},
                              separator=' ')[1:-1].strip() + '\n'

        # for fingerprint
        self.energy = 42.
        self.charge = 24.
        self.comment = 'test data set'

    def testCS(self):
        """ setup a coordinate system and check the output
        """
        cs = fp.CoordSys(self.e1, self.e2, self.e3)
        s = cs.tostring()
        # print s
        self.assertEqual(s, self.refCsString)

    def testAtom(self):
        """ setup an atom and check the output
        """
        atom = fp.Atom(self.Pos, self.AtomType, self.c,
                       self.Force)
        s = atom.tostring()
        self.assertEqual(s, self.refAtomString)

    def testAtomicStructureBase(self):
        """ setup a fingerprint and check the output
        """
        cs = fp.CoordSys(self.e1, self.e2, self.e3)
        atom = fp.Atom(self.Pos, self.AtomType, self.c,
                       self.Force)
        FingerPrint = fp.AtomicStructureBase(cs, [atom], self.energy, self.elements,
                                             self.charge, self.comment)
        FingerPrint.write('AtomicStructure.test', writepolicy='w')

        self.assertTrue(filecmp.cmp('AtomicStructure.ref',
                                    'AtomicStructure.test'))


if __name__ == '__main__':
    unittest.main()
