#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@file   03_TestVASPaseInput.py

@author Markus Stricker <markus.stricker@epfl.edu>

@date   2019-01-10

@brief

@section LICENCE

Copyright (C) 2018 Markus Stricker

03_TestVASPaseInput.py is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

03_TestVASPInput.py is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licences/>.
"""

try:
    import unittest
    import filecmp

    import StructureForge.InputTools.VASPReaderASE as vr
    import StructureForge.OutputTools.AtomicStructure as fp

except ImportError as err:
    import sys
    print(err)
    sys.exit(-1)


class TestVASPaseReader(unittest.TestCase):
    def setUp(self):
        # file
        self.folder_single = './VASP_example_1/'
        self.folder_double = './VASP_example_2/'
        self.folder_triple1 = './VASP_example_3/y_E_V/y_dir/0.980/'
        self.folder_triple2 = './VASP_example_3/y_E_V/y_dir/0.986/'
        self.folder_vasp4 = './VASP_example_4/'

        # For comparison
        self.eps = 1e-14

    def testVASPReadSingleElement(self):
        atomicStructure = vr.reader(self.folder_single, units="atomic")

        for a in atomicStructure.atoms:
            self.assertIsInstance(a, fp.Atom)
            self.assertEqual(a.AtomType, 'Mg')
            self.assertEqual(a.c[0], 0.0)
            self.assertEqual(a.c[1], 0.0)

        atomicStructure.write(self.folder_single + 'AtomicStructureVASP.test',
                              writepolicy='w')
        self.assertTrue(
            filecmp.cmp(
                self.folder_single +
                'AtomicStructureVASPase.ref',
                self.folder_single +
                'AtomicStructureVASP.test'))

    def testVASPReadDoubleElement(self):
        atomicStructure = vr.reader(self.folder_double, units="atomic")

        for i, a in enumerate(atomicStructure.atoms):
            self.assertIsInstance(a, fp.Atom)
            if i < 35:
                self.assertEqual(a.AtomType, 'Mg')
            else:
                self.assertEqual(a.AtomType, 'Y')
            self.assertEqual(a.c[0], 0.0)
            self.assertEqual(a.c[1], 0.0)

        atomicStructure.write(self.folder_double + 'AtomicStructureVASP.test',
                              writepolicy='w')
        self.assertTrue(
            filecmp.cmp(
                self.folder_double +
                'AtomicStructureVASPase.ref',
                self.folder_double +
                'AtomicStructureVASP.test'))

    def test_VASPReaderTriple1Element(self):
        atomicStructure = vr.reader(self.folder_triple1, units="atomic")

        for i, a in enumerate(atomicStructure.atoms):
            self.assertIsInstance(a, fp.Atom)

            if i < 12:
                self.assertEqual(a.AtomType, 'Al')
            elif i > 11 and i < 20:
                self.assertEqual(a.AtomType, 'Cu')
            else:
                self.assertEqual(a.AtomType, 'Li')

            self.assertEqual(a.c[0], 0.0)
            self.assertEqual(a.c[1], 0.0)

        atomicStructure.write(self.folder_triple1 + 'AtomicStructureVASP.test',
                              writepolicy='w')
        self.assertTrue(filecmp.cmp(
            self.folder_triple1 + 'AtomicStructureVASPase.ref',
            self.folder_triple1 + 'AtomicStructureVASP.test'))

    def test_VASPReaderTriple2Element(self):
        atomicStructure = vr.reader(self.folder_triple2, units="atomic")

        for i, a in enumerate(atomicStructure.atoms):
            self.assertIsInstance(a, fp.Atom)

            if i < 12:
                self.assertEqual(a.AtomType, 'Al')
            elif i > 11 and i < 20:
                self.assertEqual(a.AtomType, 'Cu')
            else:
                self.assertEqual(a.AtomType, 'Li')

            self.assertEqual(a.c[0], 0.0)
            self.assertEqual(a.c[1], 0.0)

        atomicStructure.write(self.folder_triple2 + 'AtomicStructureVASP.test',
                              writepolicy='w')
        self.assertTrue(filecmp.cmp(
            self.folder_triple2 + 'AtomicStructureVASPase.ref',
            self.folder_triple2 + 'AtomicStructureVASP.test'))

    def test_VASPReader4(self):
        atomicStructure = vr.reader(self.folder_vasp4, units="atomic")

        for i, a in enumerate(atomicStructure.atoms):
            self.assertIsInstance(a, fp.Atom)
            if i < 35:
                self.assertEqual(a.AtomType, 'Mg')
            else:
                self.assertEqual(a.AtomType, 'Y')
            self.assertEqual(a.c[0], 0.0)
            self.assertEqual(a.c[1], 0.0)

        atomicStructure.write(self.folder_vasp4 + 'AtomicStructureVASP.test',
                              writepolicy='w')
        self.assertTrue(
            filecmp.cmp(
                self.folder_vasp4 +
                'AtomicStructureVASPase.ref',
                self.folder_vasp4 +
                'AtomicStructureVASP.test'))


if __name__ == '__main__':
    unittest.main()
