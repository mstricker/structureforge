#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@file   01_TestImport.py

@author Markus Stricker <markus.stricker@epfl.edu>

@date   2018-03-28

@brief

@section LICENCE

Copyright (C) 2018 Markus Stricker

01_TestImport.py is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

01_TestImport.py is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licences/>.
"""

try:
    import unittest
    import importlib
    import numpy as np
except ImportError as err:
    import sys
    print(err)
    sys.exit(-1)


class ImportabilityChecks(unittest.TestCase):

    def import_module(self, module):
        return_code = -1
        try:
            importlib.import_module(module)
            return_code = 0
        except ImportError:
            pass
        return return_code

    def test_VingrPrintr(self):
        self.assertEqual(self.import_module("StructureForge"), 0)

    def test_OutputTools(self):
        self.assertEqual(self.import_module("StructureForge.OutputTools"), 0)


if __name__ == '__main__':
    unittest.main()
