#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@file   StructureForge

@author Markus Stricker <markus.stricker@epfl.edu>

@date   2018-03-29

@brief  Reader for VASP data files

@section LICENCE

Copyright (C) 2018 Markus Stricker

StructureForge is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

StructureForge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licences/>.

-------------------------

Necessary information to build up a fingerprint:
- lattice unit vectors [Bohr]
- atom position [Bohr], type [string], charges = [0., 0.], force [Hartree/bar]
- total energy [Hartree]
- total charge = 0.0
- comment [string], type of simulations/number whatever

VASP:
https://cms.mpi.univie.ac.at/vasp/vasp/Files_used_VASP.html
forces on atoms: OUTCAR
convergence of total energy: OSZICAR
lattice parameter: CONTCAR
ionic positions: XDATCAR

energy reference: POTCAR
k-point coordinates: KPOINTS
lattice geometry and ionic positions: POSCAR

Binglun ->
files to open in simulation folder:
CONTCAR
OUTCAR
"""

import numpy as np
import ase
from StructureForge.OutputTools import AtomicStructure as fp


def reader(folder, units=None):
    """A reader for VASP CONTCAR and OUTCAR files to convert to
    AtomicStructure
    folder -- string to simulation folder
    compare_pos -- compare ionic positions of OUTCAR and CONTCAR files.

    Length units of VASP output are in Angstroms.
    Exception is the presence of the keyword 'Direct'. Then the
    atom positions are given in units of the lattice.
    """

    print("Loading VASP data from {0}".format(folder))

    if units == "atomic":
        length_conv = 1. / ase.units.Bohr
        force_conv = ase.units.Bohr / ase.units.Hartree
        energy_conv = 1. / ase.units.Hartree
    else:
        length_conv = 1.
        force_conv = 1.
        energy_conv = 1.

    print("length ", length_conv)
    print("force ", force_conv)

    fCONTCAR = open(folder + 'CONTCAR', 'r')
    fOUTCAR = open(folder + 'OUTCAR', 'r')

    lCONT = fCONTCAR.readlines()
    lOUT = fOUTCAR.readlines()

    fCONTCAR.close()
    fOUTCAR.close()

    # CONTCAR info
    a0 = float(lCONT[1])
    a1 = np.asarray(lCONT[2].split(), dtype=np.float64)
    a2 = np.asarray(lCONT[3].split(), dtype=np.float64)
    a3 = np.asarray(lCONT[4].split(), dtype=np.float64)

    v1 = a1 * a0 * length_conv
    v2 = a2 * a0 * length_conv
    v3 = a3 * a0 * length_conv

    cs = fp.CoordSys(v1, v2, v3)

    
    elements = lCONT[5].split() # list of elements, e.g. "Zr H"
    nelements = len(elements)   # number of distinct elements

    # number of elements per species
    atoms_per_element_in = lCONT[6].split()
    natoms_per_element = [int(x) for x in atoms_per_element_in]
    ntot = sum(natoms_per_element)
    
    # means VASP 5.0 format
    # if elements[0].isalpha():
    #     nelements = [int(x) for x in lCONT[6].split()]
    #     doff = 6
    # means VASP 4.0 format
    # else:
    #     nelements = [int(x) for x in elements]
    #     elements = ['Element' + str(x + 1) for x in range(len(elements))]
    #     doff = 5
    #     nelem = len(elements)
    #     n = 0
    #     for l in lOUT:
    #         if len(l.split()) != 0:
    #             if l.split()[0] == 'POTCAR:':
    #                 if n < nelem:
    #                     elements[n] = l.split()[2]
    #                     n += 1

    # print("nelements ", nelements)
    # print("number of atoms per element = {}".format(natoms_per_element))

    # print("Elements {}".format(elements))
    # print("No of Elements {}".format(nelements))

    atom_elements = []
    for i, natoms in enumerate(natoms_per_element):
        atom_species_tmp = [elements[i] for natom in range(natoms)]
        atom_elements += atom_species_tmp

    doff = 6

    if lCONT[7].split()[0] == "Selective":
        doff += 1

    unit_key = lCONT[doff + 1].split()[0]

    doff += 2
    atompos = lCONT[doff:doff + ntot]
    doff += 1 + ntot

    atoms1 = []
    for i in range(ntot):
        postmp = np.asarray(atompos[i].split()[:3], dtype=np.float64)
        if unit_key == 'Direct':
            pos = postmp[0] * v1 + postmp[1] * v2 \
                + postmp[2] * v3
        elif unit_key == 'Cartesian':
            pos = postmp * length_conv

        atype = atom_elements[i]
        force = np.zeros(3) * force_conv
        charge = [0., 0.]
        atom = fp.Atom(pos, atype, c=charge, Force=force)
        atoms1.append(atom)

    # OUTCAR info
    lout_rev = lOUT[::-1]

    for row, l in enumerate(lout_rev):
        if l.split():
            if l.split()[0] == 'POSITION':
                break

    atoms2 = []
    offset = row - 2
    for i in range(ntot):
        postmp = np.asarray(
            lout_rev[offset - i].split()[0:3], dtype=np.float64)
        forcetmp = np.asarray(
            lout_rev[offset - i].split()[3:], dtype=np.float64)
        postmp *= length_conv
        forcetmp *= force_conv

        atype = atom_elements[i]
        charge = [0., 0.]

        atom = fp.Atom(postmp, atype, c=charge, Force=forcetmp)
        atoms2.append(atom)

    # compare atom positions from two versions
    for i in range(ntot):
        a1 = atoms1[i]
        a2 = atoms2[i]
        # Change to more precise position, if they differ
        # with respect to the precision for write-out
        if np.linalg.norm(a1.Pos - a2.Pos) < 1e-5:
            atoms2[i].Pos = a1.Pos
        else:
            print("Position error = {}\n Using position from OUTCAR".
                  format(np.linalg.norm(a1.Pos - a2.Pos)))

    # energy
    for row, l in enumerate(lout_rev):
        if l.split():
            ls = l.split()
            if ls[0] == 'energy' \
               and ls[1] == 'without' \
               and ls[2] == 'entropy=':
                break

    energy = float(lout_rev[row].split()[-1]) * energy_conv
    comment = folder  # change to folder

    return fp.AtomicStructureBase(cs, atoms2, energy, elements, comment=comment)

def reader_poscar(folder, units=None):
    """A reader for VASP CONTCAR and OUTCAR files to convert to
    AtomicStructure
    folder -- string to simulation folder
    compare_pos -- compare ionic positions of OUTCAR and CONTCAR files.

    Length units of VASP output are in Angstroms.
    Exception is the presence of the keyword 'Direct'. Then the
    atom positions are given in units of the lattice.
    """

    print("Loading VASP data from {0}".format(folder))

    if units == "atomic":
        length_conv = 1. / ase.units.Bohr
        force_conv = ase.units.Bohr / ase.units.Hartree
        energy_conv = 1. / ase.units.Hartree
    else:
        length_conv = 1.
        force_conv = 1.
        energy_conv = 1.

    # print("length ", length_conv)
    # print("force ", force_conv)

    fCONTCAR = open(folder + 'POSCAR', 'r')
    fOUTCAR = open(folder + 'OUTCAR', 'r')

    lCONT = fCONTCAR.readlines()
    lOUT = fOUTCAR.readlines()

    fCONTCAR.close()
    fOUTCAR.close()

    # CONTCAR info
    a0 = float(lCONT[1])
    a1 = np.asarray(lCONT[2].split(), dtype=np.float64)
    a2 = np.asarray(lCONT[3].split(), dtype=np.float64)
    a3 = np.asarray(lCONT[4].split(), dtype=np.float64)

    v1 = a1 * a0 * length_conv
    v2 = a2 * a0 * length_conv
    v3 = a3 * a0 * length_conv

    cs = fp.CoordSys(v1, v2, v3)

    
    elements = lCONT[5].split() # list of elements, e.g. "Zr H"
    nelements = len(elements)   # number of distinct elements

    # number of elements per species
    atoms_per_element_in = lCONT[6].split()
    natoms_per_element = [int(x) for x in atoms_per_element_in]
    ntot = sum(natoms_per_element)
    
    # print("nelements ", nelements)
    # print("number of atoms per element = {}".format(natoms_per_element))

    # print("Elements {}".format(elements))
    # print("No of Elements {}".format(nelements))

    atom_elements = []
    for i, natoms in enumerate(natoms_per_element):
        atom_species_tmp = [elements[i] for natom in range(natoms)]
        atom_elements += atom_species_tmp

    doff = 6

    if lCONT[7].split()[0] == "Selective":
        doff += 1

    unit_key = lCONT[doff + 1].split()[0]

    doff += 2
    atompos = lCONT[doff:doff + ntot]
    doff += 1 + ntot

    atoms1 = []
    for i in range(ntot):
        postmp = np.asarray(atompos[i].split()[:3], dtype=np.float64)
        if unit_key == 'Direct':
            pos = postmp[0] * v1 + postmp[1] * v2 \
                + postmp[2] * v3
        elif unit_key == 'Cartesian':
            pos = postmp * length_conv

        atype = atom_elements[i]
        force = np.zeros(3) * force_conv
        charge = [0., 0.]
        atom = fp.Atom(pos, atype, c=charge, Force=force)
        atoms1.append(atom)

    # OUTCAR info
    lout_rev = lOUT[::-1]

    for row, l in enumerate(lout_rev):
        if l.split():
            if l.split()[0] == 'POSITION':
                break

    atoms2 = []
    offset = row - 2
    for i in range(ntot):
        postmp = np.asarray(
            lout_rev[offset - i].split()[0:3], dtype=np.float64)
        forcetmp = np.asarray(
            lout_rev[offset - i].split()[3:], dtype=np.float64)
        postmp *= length_conv
        forcetmp *= force_conv

        atype = atom_elements[i]
        charge = [0., 0.]

        atom = fp.Atom(postmp, atype, c=charge, Force=forcetmp)
        atoms2.append(atom)

    # compare atom positions from two versions
    for i in range(ntot):
        a1 = atoms1[i]
        a2 = atoms2[i]
        # Change to more precise position, if they differ
        # with respect to the precision for write-out
        if np.linalg.norm(a1.Pos - a2.Pos) < 1e-5:
            atoms2[i].Pos = a1.Pos
        else:
            print("Position error = {}\n Using positions from OUTCAR"
                  .format(np.linalg.norm(a1.Pos - a2.Pos)))
            print("a1, a2 = {}/{}".format(a1.Pos, a2.Pos))
        # else:
        #     print("Position error = {}".format(np.linalg.norm(a1.Pos - a2.Pos)))
        #     raise ValueError("OUTCAR and CONTCAR positions do not match.")
            

    # energy
    for row, l in enumerate(lout_rev):
        if l.split():
            ls = l.split()
            if ls[0] == 'energy' \
               and ls[1] == 'without' \
               and ls[2] == 'entropy=':
                break

    energy = float(lout_rev[row].split()[-1]) * energy_conv
    comment = folder  # change to folder

    return fp.AtomicStructureBase(cs, atoms2, energy, elements, comment=comment)
