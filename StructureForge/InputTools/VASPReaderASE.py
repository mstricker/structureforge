#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@file   StructureForge

@author Markus Stricker <markus.stricker@epfl.edu>

@date   2018-03-29

@brief  Reader for VASP data files

@section LICENCE

Copyright (C) 2018 Markus Stricker

StructureForge is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

StructureForge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licences/>.

-------------------------

Necessary information to build up a fingerprint:
- lattice unit vectors [Bohr]
- atom position [Bohr], type [string], charges = [0., 0.], force [Hartree/bar]
- total energy [Hartree]
- total charge = 0.0
- comment [string], type of simulations/number whatever

VASP:
https://cms.mpi.univie.ac.at/vasp/vasp/Files_used_VASP.html
forces on atoms: OUTCAR
convergence of total energy: OSZICAR
lattice parameter: CONTCAR
ionic positions: XDATCAR

energy reference: POTCAR
k-point coordinates: KPOINTS
lattice geometry and ionic positions: POSCAR

Binglun ->
files to open in simulation folder:
CONTCAR
OUTCAR
"""

from StructureForge.OutputTools import AtomicStructure as fp

from ase.io import read
import ase


def reader(folder, units=None):
    """A reader for VASP OUTCAR to convert
    AtomicStructure
    folder -- string to simulation folder

    Length units of VASP output are in Angstroms.
    Exception is the presence of the keyword 'Direct'. Then the
    atom positions are given in units of the lattice.
    """

    print("Loading VASP data from {0}".format(folder))

    if units == "atomic":
        length_conv = 1. / ase.units.Bohr
        force_conv = ase.units.Bohr / ase.units.Hartree
        energy_conv = 1. / ase.units.Hartree
    else:
        length_conv = 1.
        force_conv = 1.
        energy_conv = 1.

    print("length conv ", length_conv)
    print("force conv  ", force_conv)

    frame = read(folder + "OUTCAR", format="vasp-out")

    # CONTCAR info
    v1 = frame.cell[0, :] * length_conv
    v2 = frame.cell[1, :] * length_conv
    v3 = frame.cell[2, :] * length_conv

    cs = fp.CoordSys(v1, v2, v3)

    # elements = set(
    atoms = []
    elements = []
    for atom in frame:
        pos = atom.position * length_conv
        index = atom.index
        element = frame.get_chemical_symbols()[index]
        if element not in elements:
            elements.append(element)
        force = frame.get_forces()[index, :] * force_conv
        # manually set charge to zero
        charge = [0., 0.]
        atom = fp.Atom(pos, element, c=charge, Force=force)
        atoms.append(atom)

    energy = frame.get_potential_energy() * energy_conv
    comment = folder
    
    print ("folder, energy, elements", folder, energy, elements)

    return fp.AtomicStructureBase(cs, atoms, energy, elements, comment=comment)
