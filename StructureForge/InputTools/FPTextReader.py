#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@file   StructureForge

@author Markus Stricker <markus.stricker@epfl.edu>

@date   2018-05-02

@brief  Reader for VASP data files

@section LICENCE

Copyright (C) 2018 Markus Stricker

StructureForge is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

StructureForge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licences/>.
"""

import numpy as np

from StructureForge.OutputTools import AtomicStructure as fp


def reader(filename):
    """
    Reads from a text file of fingerprints separated by begin/end and returns a
    list.
    Args:
        filename : string to file with path/relative
    Returns:
        fplist : a list of type AtomicStructure
    """
    fplist = []

    f = open(filename, 'r')
    lines = f.read().splitlines()
    f.close()

    def parse_fp(lines):
        comment = lines[0][2:]

        v1 = np.asarray(lines[1].split(' ')[1:], dtype=np.float64)
        v2 = np.asarray(lines[2].split(' ')[1:], dtype=np.float64)
        v3 = np.asarray(lines[3].split(' ')[1:], dtype=np.float64)
        cs = fp.CoordSys(v1, v2, v3)

        charge = float(lines[-1].split(' ')[1])
        energy = float(lines[-2].split(' ')[1])

        atoms = []
        elements = []
        for l in lines[4:-2]:
            list = [s for s in l.split(' ') if s]
            pos = np.asarray(list[1:4], dtype=np.float64)
            atype = list[4]
            charge = [float(list[5]), float(list[6])]
            force = np.asarray(list[7:], dtype=np.float64)
            atom = fp.Atom(pos, atype, c=charge, Force=force)
            atoms.append(atom)
            if atype not in elements:
                elements.append(atype)
            

        return fp.AtomicStructureBase(cs, atoms, energy, elements, comment=comment)

    for i, l in enumerate(lines):
        if l.split()[0] == 'begin':
            i1 = i + 1

        elif l.split()[0] == 'end':
            i2 = i
            fplist.append(parse_fp(lines[i1:i2]))

    return fplist
