#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@file   StructureForge

@author Markus Stricker <markus.stricker@epfl.edu>

@date   2018-03-28

@brief

@section LICENCE

Copyright (C) 2018 Markus Stricker

StructureForge is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

StructureForge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licences/>.
"""


"""
install this system-wide
python setup.py install --use
"""
