#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@file   AtomicStructure.py

@author Markus Stricker <markus.stricker@epfl.edu>

@date   2018-03-28

@brief  Class structure for atomic fingerprint output.

@section LICENCE

Copyright (C) 2018 Markus Stricker

StructureForge is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

StructureForge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licences/>.
"""

import numpy as np


class CoordSys(object):
    "Class for storing lattice unit vectors."

    def __init__(self, a1, a2, a3):
        """
        a_i -- 3D np.arrays defining the unit cell of the lattice in units of
              [Bohr]
        """
        self.a1 = a1
        self.a2 = a2
        self.a3 = a3

    def tostring(self):
        """Gives back a string of the info to write to file"""
        s = 'lattice ' \
            + np.array2string(self.a1, precision=8,
                              formatter={'float_kind': lambda x: "%.8f" % x},
                              separator=' ')[1:-1].strip() + '\n' \
            + 'lattice ' \
            + np.array2string(self.a2, precision=8,
                              formatter={'float_kind': lambda x: "%.8f" % x},
                              separator=' ')[1:-1].strip() + '\n' \
            + 'lattice ' \
            + np.array2string(self.a3, precision=8,
                              formatter={'float_kind': lambda x: "%.8f" % x},
                              separator=' ')[1:-1].strip() + '\n'
        return s


class Atom(object):
    "Class for storing individual atom data."

    def __init__(self, Pos, AtomType, c=[0., 0.],
                 Force=np.array([0., 0., 0.])):
        """ Represents everything that is needed for one atom line of
        information.
        Keyword Arguments:
        Pos      -- Position vector np.array with 3 entries in units of [Bohr]
        AtomType -- String of atom type, e.g. 'Al'
        c        -- list of 2 charges (for future)
        Force    -- Force vector of atom, np.array, 3 entries in units of
                    [Hartree/Bar]
        """
        self.Pos = Pos
        self.AtomType = AtomType
        self.c = c
        self.Force = Force

    def tostring(self):
        """Returns a string of the info of one atom to write to file"""
        s = 'atom  ' \
            + np.array2string(self.Pos,
                              formatter={'float_kind': lambda x: "%.8f" % x},
                              separator=' ')[1:-1].strip() \
            + '  ' + self.AtomType + ' ' + str(self.c[0]) + ' ' \
            + str(self.c[1]) + '  ' \
            + np.array2string(self.Force,
                              formatter={'float_kind': lambda x: "%.8e" % x},
                              separator=' ')[1:-1].strip() + '\n'
        return s


class AtomicStructureBase(object):
    "Base class for one atomic structure."

    def __init__(self, lattice, atoms, energy, elements, charge=0.0,
                 comment='comment text'):
        """ Represents everything that goes into the output of a fingerprint.
        Keyword Arguments:
        lattice  -- An instance of CoordSys. Defines the repeat vectors of a
                   lattice in units length
        atoms    -- A list of instances of class Atom with positions, type, 2
                  charge contributions (not yet implemented) and forces.
        energy   -- Total energy of the system in units of energy
        elements -- list of strings of elements, e.g. ['Mg', 'Y']
        charge   -- A value of charge of the system, not yet implemented
        """
        self.lattice = lattice
        self.atoms = atoms
        self.energy = float(energy)
        self.elements = elements
        self.charge = float(charge)
        self.comment = str(comment)

    def write(self, fname='atomicstructures.out', writepolicy='w'):
        """Function for writing the information into the file 'atomicstructures.out'
        fname -- filename for writing the information,
        """

        with open(fname, writepolicy) as f:
            f.write('begin\n')
            comment_string = "comment " + self.comment
            f.write(comment_string + '\n')
            f.write(self.lattice.tostring())
            for a in self.atoms:
                f.write(a.tostring())
            f.write('energy ' + "{:.12f}".format(self.energy) + '\n')
            f.write('charge ' + "{:.12f}".format(self.charge) + '\n')
            f.write('end' + '\n')

    def print_stdout(self):
        """Output one atomic structures on the console"""
        print ('>>')
        print ('AtomicStructure comment', self.comment)
        print ('Coordinate system')
        print (self.lattice.tostring())
        for a in self.atoms:
            print (a.tostring())
        print ('Energy', self.energy)
        print ('Charge', self.charge)
        print ('<<')
